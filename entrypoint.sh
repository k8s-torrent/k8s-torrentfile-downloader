#!/bin/sh -xe

TORRENTFILE_PATH=/tmp/torrentfile
COLLECTOR=$1

echo "Downloading $COLLECTOR"
TORRENTFILE=$(curl $COLLECTOR/torrentfile | jq -r '.Location')
echo $TORRENTFILE
curl -o $TORRENTFILE_PATH $TORRENTFILE
curl -F "torrentfile=@$TORRENTFILE_PATH" $COLLECTOR/torrentfile
echo "Done"
