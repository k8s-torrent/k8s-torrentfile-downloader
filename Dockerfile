FROM alpine

RUN apk --no-cache add curl jq
ADD . /usr/src/app
WORKDIR /usr/src/app
